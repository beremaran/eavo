package entities

import (
	"github.com/jinzhu/copier"
	"gitlab.com/beremaran/eavo/domain/types"
)

//BoxVariation encapsulates a Box with heuristic extras
type BoxVariation struct {
	Box      *Box
	Rotation types.Axis
}

//Equals checks if a variation equals to another
func (bv *BoxVariation) Equals(o *BoxVariation) bool {
	return bv.Box.ID == o.Box.ID && bv.Rotation == o.Rotation
}

//ToBox finalizes variation as Box instance
func (bv *BoxVariation) ToBox() *Box {
	box := &Box{}
	err := copier.Copy(box, bv.Box)
	if err != nil {
		panic(err)
	}

	return box.Rotate(bv.Rotation)
}
