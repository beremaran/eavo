package entities

import "gitlab.com/beremaran/eavo/domain/types"

//Container encapsulates the properties of a Container entity
type Container struct {
	Size types.Vector3i
}

//Volume returns volume of the container
func (c *Container) Volume() int {
	return c.Size.X * c.Size.Y * c.Size.Z
}

//ToBox returns a Box with same size of Container
func (c *Container) ToBox() Box {
	return Box{Size: c.Size}
}

//Center returns center point of the container
func (c *Container) Center() types.Vector3i {
	return types.Vector3i{
		X: c.Size.X / 2,
		Y: c.Size.Y / 2,
		Z: c.Size.Z / 2,
	}
}
