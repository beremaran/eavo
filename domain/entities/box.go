package entities

import (
	"crypto/md5"
	"encoding/json"
	"github.com/jinzhu/copier"
	"gitlab.com/beremaran/eavo/domain/types"
)

//Box encapsulates properties of a Box entity
type Box struct {
	ID              int
	Size            types.Vector3i
	Position        types.Vector3i
	RotationAllowed types.Vector3b

	hash    [16]byte
	corners []types.Vector3i
}

//GetAxisLength returns the length of a given axis
func (b *Box) GetAxisLength(axis types.Axis) int {
	switch axis {
	case types.AxisX:
		return b.Size.X
	case types.AxisY:
		return b.Size.Y
	case types.AxisZ:
		return b.Size.Z
	}

	return -1
}

//Weight returns weight of the box
func (b *Box) Weight() int {
	return b.Volume()
}

//Volume returns the volume of the box
func (b *Box) Volume() int {
	return b.Size.X * b.Size.Y * b.Size.Z
}

//Clone creates a independent copy (deep-copy) from box
func (b *Box) Clone() *Box {
	box := &Box{}
	err := copier.Copy(box, b)
	if err != nil {
		panic(err)
	}

	return box
}

//Fits returns true if box fits into given box
func (b *Box) Fits(o *Box) bool {
	rotations := []*Box{
		b,
		b.Rotate(types.AxisX),
		b.Rotate(types.AxisY),
		b.Rotate(types.AxisZ),
	}

	for i := 0; i < len(rotations); i++ {
		r := rotations[i]

		if r.Size.X <= o.Size.X &&
			r.Size.Y <= o.Size.Y &&
			r.Size.Z <= o.Size.Z {
			return true
		}
	}

	return false
}

//Rotate returns rotated version of Box
func (b *Box) Rotate(axis types.Axis) *Box {
	rotated := &Box{}
	err := copier.Copy(rotated, b)
	if err != nil {
		panic(err)
	}

	switch axis {
	case types.AxisX:
		rotated.Size.Y, rotated.Size.Z = rotated.Size.Z, rotated.Size.Y
		break
	case types.AxisY:
		rotated.Size.X, rotated.Size.Z = rotated.Size.Z, rotated.Size.X
		break
	case types.AxisZ:
		rotated.Size.X, rotated.Size.Y = rotated.Size.Y, rotated.Size.X
		break
	}

	return rotated
}

//IsSameSized checks if it is same sized (and/or with rotation)
func (b *Box) IsSameSized(o *Box) bool {
	rotations := []*Box{
		b,
		b.Rotate(types.AxisX),
		b.Rotate(types.AxisY),
		b.Rotate(types.AxisZ),
	}

	for i := 0; i < len(rotations); i++ {
		r := rotations[i]

		if r.Size.Equals(&o.Size) {
			return true
		}
	}

	return false
}

//DefaultVariation returns default variation with AxisNone rotation
func (b *Box) DefaultVariation() *BoxVariation {
	return &BoxVariation{
		Box:      b,
		Rotation: types.AxisNone,
	}
}

//CurrentHash calculates current hash of corner points
func (b *Box) CurrentHash() [16]byte {
	var arrBytes []byte

	jsonBytes, _ := json.Marshal(b.Size)
	arrBytes = append(arrBytes, jsonBytes...)

	jsonBytes, _ = json.Marshal(b.Position)
	arrBytes = append(arrBytes, jsonBytes...)

	return md5.Sum(arrBytes)
}

//Corners calculates corners of box
func (b *Box) Corners() []types.Vector3i {
	//if b.corners == nil {
	cornerOffsets := [][]int{
		{0, 0, 0},
		{b.Size.X, 0, 0},
		{0, b.Size.Y, 0},
		{0, 0, b.Size.Z},
		{b.Size.X, b.Size.Y, 0},
		{b.Size.X, 0, b.Size.Z},
		{0, b.Size.Y, b.Size.Z},
		{b.Size.X, b.Size.Y, b.Size.Z},
	}

	b.corners = nil
	for _, offset := range cornerOffsets {
		corner := types.Vector3i{
			X: b.Position.X + offset[0],
			Y: b.Position.Y + offset[1],
			Z: b.Position.Z + offset[2],
		}

		b.corners = append(b.corners, corner)
	}

	//	b.hash = b.CurrentHash()
	//}

	return b.corners
}

func (b *Box) isHashChanged() bool {
	currentHash := b.CurrentHash()

	for i := 0; i < 16; i++ {
		if b.hash[i] != currentHash[i] {
			return true
		}
	}

	return false
}

//Center calculates center point of the box
func (b *Box) Center() types.Vector3i {
	return types.Vector3i{
		X: b.Position.X + (b.Size.X / 2),
		Y: b.Position.Y + (b.Size.Y / 2),
		Z: b.Position.Z + (b.Size.Z / 2),
	}
}
