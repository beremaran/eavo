package entities

//Problem encapsulates a loading problem's context
type Problem struct {
	ID        int
	Seed      int
	Boxes     []Box
	Container Container
}
