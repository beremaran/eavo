package genetic

import "gitlab.com/beremaran/eavo/domain/entities"

//Decoder decode a given list of BoxVariations
type Decoder interface {
	SetContext(ctx *Context)
	Decode([][]*entities.BoxVariation) []*entities.Box
	Clone() Decoder
}
