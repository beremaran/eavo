package genetic

//Mutation applies mutation operator to an Individual
type Mutation interface {
	SetContext(ctx Context)
	GetProbability() float64
	Mutate(individual *Individual) error
}
