package genetic

import (
	"errors"
	"fmt"
	"gitlab.com/beremaran/eavo/domain/entities"
	"log"
	"math/rand"
	"sync"
	"time"
)

//Statistics stores information about the generation iteration
type Statistics struct {
	GenerationID int
	MaxFitness   float64
	AvgFitness   float64
	MinFitness   float64
	Duration     time.Duration
	BestLayout   []*entities.Box
}

//Optimizer runs the genetic process, optimizing its population
//generation by generation
type Optimizer struct {
	Context          Context
	Decoder          Decoder
	Selector         Selector
	Mutations        []Mutation
	FitnessFunctions []FitnessFunction
	Generator        GeneratorFunction
	Crossover        CrossoverFunction
	History          []Statistics

	population           []*Individual
	bestIndividual       *Individual
	noProgressCounter    int
	lastBestFitnessScore float64
}

//NewOptimizer creates a new Optimizer instance with given configuration
func NewOptimizer(context Context, decoder Decoder, selector Selector, generator GeneratorFunction, crossover CrossoverFunction) *Optimizer {
	optimizer := &Optimizer{Context: context, Decoder: decoder, Selector: selector, Generator: generator, Crossover: crossover}
	generator.SetContext(&optimizer.Context)
	decoder.SetContext(&optimizer.Context)
	return optimizer
}

//Optimize optimizes its Context.Problem by using genetic processes
func (o *Optimizer) Optimize() ([]*entities.Box, []Statistics, error) {
	o.History = nil
	rand.Seed(int64(o.Context.Problem.Seed))

	var err error
	var generation int

	err = o.generatePopulation()
	if err != nil {
		return nil, nil, err
	}

	err = o.calculatePopulationFitness()
	if err != nil {
		return nil, nil, err
	}

	generation = 1
	o.noProgressCounter = 0
	o.updateBestIndividual()
	for ; generation <= o.Context.MaxGenerations; generation++ {
		tStart := time.Now()

		err = o.step()
		if err != nil {
			return nil, nil, err
		}

		err = o.calculatePopulationFitness()
		if err != nil {
			return nil, nil, err
		}

		tEnd := time.Now()

		o.updateBestIndividual()
		o.logGeneration(generation, tEnd.Sub(tStart))
		o.rememberGeneration(generation, tEnd.Sub(tStart))

		if o.lastBestFitnessScore < o.bestIndividual.FitnessScore {
			o.lastBestFitnessScore = o.bestIndividual.FitnessScore
			o.noProgressCounter = 0
		} else {
			o.noProgressCounter++
		}

		if o.noProgressCounter >= o.Context.MaxRepeatedGenerations {
			log.Println("Cancelling further generations due to no progress.")
			break
		}
	}

	decodedBoxes := o.decode(o.bestIndividual)
	log.Printf("Placed %d out of %d\n", len(decodedBoxes), len(o.Context.Problem.Boxes))
	return decodedBoxes, o.History, nil
}

func (o *Optimizer) rememberGeneration(generationID int, duration time.Duration) {
	min, avg, max := 999.0, 0.0, -999.0

	for i := 0; i < len(o.population); i++ {
		individual := o.population[i]

		if individual.FitnessScore < min {
			min = individual.FitnessScore
		}

		if individual.FitnessScore > max {
			max = individual.FitnessScore
		}

		avg += individual.FitnessScore
	}

	avg /= float64(len(o.population))

	o.History = append(o.History, Statistics{
		Duration:     duration,
		BestLayout:   o.Decoder.Decode(o.bestIndividual.Genome),
		AvgFitness:   avg,
		GenerationID: generationID,
		MaxFitness:   max,
		MinFitness:   min,
	})
}

func (o *Optimizer) generatePopulation() error {
	if o.Generator == nil {
		return fmt.Errorf("no generator found")
	}

	for len(o.population) <= o.Context.PopulationSize {
		generated, err := o.Generator.Generate()
		if err != nil {
			return err
		}

		o.population = append(o.population, generated)
	}

	return nil
}

func (o *Optimizer) calculatePopulationFitness() error {
	wg := sync.WaitGroup{}
	totalWeight := o.getTotalFitnessScoreWeight()

	for i := 0; i < len(o.population); i++ {
		wg.Add(1)

		go func(k int) {
			defer wg.Done()

			decoder := o.Decoder.Clone()

			o.population[k].FitnessScore = 0
			decodedBoxes := decoder.Decode(o.population[k].Genome)

			for j := 0; j < len(o.FitnessFunctions); j++ {
				fitnessScore, err := o.FitnessFunctions[j].Calculate(decodedBoxes)
				if err != nil {
					log.Println(err)
				}

				o.population[k].FitnessScore += fitnessScore * o.FitnessFunctions[j].GetWeight()
			}

			o.population[k].FitnessScore /= totalWeight
		}(i)

	}

	wg.Wait()
	return nil
}

func (o *Optimizer) step() error {
	var offspring []*Individual

	if o.Context.NumberOfElites > 0 {
		if o.Context.NumberOfElites > len(o.population) {
			return errors.New("number of elites is greater than population size")
		}

		offspring = append(offspring, o.getBestIndividuals(o.Context.NumberOfElites)...)
	}

	for len(offspring) < o.Context.PopulationSize {
		var newIndividual *Individual
		var err error

		if rand.Float64() <= o.Context.CrossoverRate {
			newIndividual, err = o.stepWithCrossover()
			if err != nil {
				return err
			}
		} else {
			newIndividual, err = o.stepWithoutCrossover()
			if err != nil {
				return err
			}
		}

		offspring = append(offspring, newIndividual)
	}

	o.population = offspring
	return nil
}

func (o *Optimizer) updateBestIndividual() {
	for i := 0; i < len(o.population); i++ {
		if o.bestIndividual == nil ||
			o.population[i].FitnessScore > o.bestIndividual.FitnessScore {
			o.bestIndividual = o.population[i].Copy()
		}
	}
}

func (o *Optimizer) logGeneration(generation int, duration time.Duration) {
	log.Printf("Generation #%04d: %f (%s)", generation, o.bestIndividual.FitnessScore, duration.String())
}

func (o *Optimizer) getTotalFitnessScoreWeight() float64 {
	totalWeight := 0.0

	for i := 0; i < len(o.FitnessFunctions); i++ {
		totalWeight += o.FitnessFunctions[i].GetWeight()
	}

	return totalWeight
}

//AddMutation adds a new mutation to optimizer's mutation set
func (o *Optimizer) AddMutation(mutation Mutation) {
	mutation.SetContext(o.Context)
	o.Mutations = append(o.Mutations, mutation)
}

//AddFitnessFunction adds a new fitness function to optimizer's fitness function set
func (o *Optimizer) AddFitnessFunction(function FitnessFunction) {
	function.SetContext(&o.Context)
	o.FitnessFunctions = append(o.FitnessFunctions, function)
}

func (o *Optimizer) stepWithCrossover() (*Individual, error) {
	parent1, err := o.Selector.Select(o.population)
	if err != nil {
		return nil, err
	}

	parent2, err := o.Selector.Select(o.population)
	if err != nil {
		return nil, err
	}

	child, err := o.Crossover.Crossover(parent1, parent2)
	if err != nil && child == nil {
		return nil, err
	}

	err = o.mutate(child)
	if err != nil {
		return nil, err
	}

	return child, nil
}

func (o *Optimizer) stepWithoutCrossover() (*Individual, error) {
	individual, err := o.Selector.Select(o.population)
	if err != nil {
		return nil, err
	}

	err = o.mutate(individual)
	if err != nil {
		return nil, err
	}

	return individual, nil
}

func (o *Optimizer) mutate(individual *Individual) error {
	if len(o.Mutations) == 0 {
		return nil
	}

	if rand.Float64() <= o.Context.MutationRate {
		mutation := o.Mutations[rand.Intn(len(o.Mutations))]
		err := mutation.Mutate(individual)
		if err != nil {
			return err
		}
	}

	return nil
}

func (o *Optimizer) decode(individual *Individual) []*entities.Box {
	return o.Decoder.Decode(individual.Genome)
}

func (o *Optimizer) getBestIndividuals(i int) []*Individual {
	var bestIndividuals []*Individual

	for len(bestIndividuals) < i {
		bestIndividuals = append(bestIndividuals, o.getBestIndividual(bestIndividuals))
	}

	return bestIndividuals
}

func (o *Optimizer) getBestIndividual(except []*Individual) *Individual {
	var bestUntil *Individual

	for i := 0; i < len(o.population); i++ {
		if bestUntil == nil ||
			(o.population[i].FitnessScore > bestUntil.FitnessScore && !ContainsIndividual(except, o.population[i])) {
			bestUntil = o.population[i]
		}
	}

	return bestUntil
}
