package genetic

import (
	"github.com/jinzhu/copier"
	"gitlab.com/beremaran/eavo/domain/entities"
)

//Individual is core element of the genetic algorithm. It's what is processed
//during the optimization
type Individual struct {
	FitnessScore float64
	Genome       [][]*entities.BoxVariation
}

//Copy makes a deep-copy of an Individual
func (i *Individual) Copy() *Individual {
	individual := &Individual{}
	err := copier.Copy(individual, i)
	if err != nil {
		panic(err)
	}

	return individual
}
