package genetic

import "gitlab.com/beremaran/eavo/domain/entities"

//FitnessFunction calculates an individual's fitness score
type FitnessFunction interface {
	GetWeight() float64
	SetContext(ctx *Context)
	Calculate([]*entities.Box) (float64, error)
}
