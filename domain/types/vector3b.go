package types

//Vector3b implements a boolean vector of size of 3
type Vector3b struct {
	X bool
	Y bool
	Z bool
}
