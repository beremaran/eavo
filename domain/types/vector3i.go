package types

import "math"

//Vector3i represents a 3-dimensional integer vector
type Vector3i struct {
	X int
	Y int
	Z int
}

//Equals checks if given vector3i is equal
func (v *Vector3i) Equals(o *Vector3i) bool {
	return v.X == o.X && v.Y == o.Y && v.Z == o.Z
}

//Sum sums up given vector
func (v *Vector3i) Sum(o *Vector3i) *Vector3i {
	return &Vector3i{
		X: v.X + o.X,
		Y: v.Y + o.Y,
		Z: v.Z + o.Z,
	}
}

//Multiply multiplies with given coefficient
func (v *Vector3i) Multiply(k int) *Vector3i {
	return &Vector3i{
		X: v.X * k,
		Y: v.Y * k,
		Z: v.Z * k,
	}
}

//Divide divides by given coefficient
func (v *Vector3i) Divide(k int) *Vector3i {
	return &Vector3i{
		X: v.X / k,
		Y: v.Y / k,
		Z: v.Z / k,
	}
}

//Distance calculates the distance between given vector
func (v *Vector3i) Distance(o *Vector3i) float64 {
	return math.Sqrt(math.Pow(float64(v.X-o.X), 2) + math.Pow(float64(v.Y-o.Y), 2) + math.Pow(float64(v.Z-o.Z), 2))
}
