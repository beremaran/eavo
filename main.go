package main

import (
	"flag"
	"gitlab.com/beremaran/eavo/interfaces"
	"gitlab.com/beremaran/eavo/interfaces/cli"
	"log"
	"os"
	"runtime/pprof"
)

var flagEnableProfiling bool

func init() {
	flag.BoolVar(&flagEnableProfiling, "with-profiling", false, "enable CPU profiling")
}

func main() {
	handlers := map[string]interfaces.Handler{}
	handlers["cli"] = &cli.Handler{}

	var handlerName = "cli"
	if flag.NArg() == 1 {
		handlerName = flag.Arg(0)
	}

	handler, exists := handlers[handlerName]
	if !exists {
		log.Fatalf("Unknown handler: %s\n", handlerName)
	}

	handler.RegisterFlags()
	flag.Parse()

	if flagEnableProfiling {
		f, err := os.Create("cpu.prof")
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close()
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	handler.Handle()
}
