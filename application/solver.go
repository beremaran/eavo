package application

import (
	"gitlab.com/beremaran/eavo/domain/entities"
	"gitlab.com/beremaran/eavo/domain/genetic"
)

//Solver solves given problem and returns corresponding boxes
type Solver interface {
	Solve(problem entities.Problem) ([]*entities.Box, []genetic.Statistics, error)
}
