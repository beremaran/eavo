package interfaces

//Handler handles an user interface case
type Handler interface {
	Handle()
	RegisterFlags()
}
