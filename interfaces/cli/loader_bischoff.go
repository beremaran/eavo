package cli

import (
	"bufio"
	"gitlab.com/beremaran/eavo/domain/entities"
	"os"
	"strconv"
	"strings"
	"time"
)

//LoadBischoff loads bischoff formatted problems from a text file
func LoadBischoff(filePath string) ([]*entities.Problem, error) {
	var problemSet []*entities.Problem

	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	nProblems := readInt(scanner)
	problemSet = make([]*entities.Problem, nProblems)

	lastBoxID := 1
	for i := 0; i < nProblems; i++ {
		tag := readIntegers(scanner)
		containerSize := readIntegers(scanner)
		nBoxTypes := readInt(scanner)

		seed := int(time.Now().Unix())
		if len(tag) == 2 {
			seed = tag[1]
		}

		p := &entities.Problem{ID: tag[0], Seed: seed}

		p.Container = entities.Container{}
		p.Container.Size.X = containerSize[0]
		p.Container.Size.Y = containerSize[1]
		p.Container.Size.Z = containerSize[2]

		for j := 0; j < nBoxTypes; j++ {
			boxDef := readIntegers(scanner)[1:]

			for k := 0; k < boxDef[6]; k++ {
				box := entities.Box{
					ID: lastBoxID,
				}
				lastBoxID++

				box.Size.X = boxDef[0]
				box.RotationAllowed.X = boxDef[1] == 1

				box.Size.Y = boxDef[2]
				box.RotationAllowed.Y = boxDef[3] == 1

				box.Size.Z = boxDef[4]
				box.RotationAllowed.Z = boxDef[5] == 1

				p.Boxes = append(p.Boxes, box)
			}
		}

		problemSet[i] = p
	}

	return problemSet, nil
}

func readLine(scanner *bufio.Scanner) string {
	scanner.Scan()
	return strings.TrimSpace(scanner.Text())
}

func readInt(scanner *bufio.Scanner) int {
	line := readLine(scanner)

	n, _ := strconv.ParseInt(line, 0, 64)
	return int(n)
}

func readIntegers(scanner *bufio.Scanner) []int {
	line := readLine(scanner)
	tokens := strings.Split(line, " ")

	var numbers []int

	for _, token := range tokens {
		n, _ := strconv.ParseInt(token, 0, 64)
		numbers = append(numbers, int(n))
	}

	return numbers
}
