package cli

import (
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/beremaran/eavo/application"
	"gitlab.com/beremaran/eavo/domain/entities"
	"log"
	"os"
)

//Handler handles command line interface
type Handler struct {
	problemID       int
	problemPackFile string
	solverAlgorithm string
}

//RegisterFlags registers CLI mode flags
func (h *Handler) RegisterFlags() {
	flag.StringVar(&h.problemPackFile, "pack", "./thpack1.txt", "path to problem pack file")
	flag.StringVar(&h.solverAlgorithm, "solver", "*", "which algorithm to solve problem with")
	flag.IntVar(&h.problemID, "problem", 1, "ID of the problem to solve in given pack")
}

//Handle handles CLI case
func (h *Handler) Handle() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	solver, err := application.GetSolver(h.solverAlgorithm)
	if err != nil {
		log.Fatal(err)
	}

	problem, err := h.getProblem()
	if err != nil {
		log.Fatal(err)
	}

	boxes, _, err := solver.Solve(*problem)
	if err != nil {
		log.Fatal(err)
	}

	/*
		result := map[string]interface{}{}

		result["ProblemId"] = fmt.Sprintf("thpack1-%s-%d", h.solverAlgorithm, h.problemID)
		result["PackName"] = "thpack1"
		result["DecoderType"] = h.solverAlgorithm
		result["Problem"] = problem

		solution := map[string]interface{}{}
		solution["PackName"] = "thpack1"
		solution["ProblemId"] = result["ProblemId"]
		solution["GeneticHistory"] = history
		solution["Layout"] = boxes

		result["Solution"] = solution

	*/
	h.dumpAsJSON(boxes)
}

func (h *Handler) getProblem() (*entities.Problem, error) {
	problems, err := LoadBischoff(h.problemPackFile)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(problems); i++ {
		if problems[i].ID == h.problemID {
			return problems[i], nil
		}
	}

	return nil, fmt.Errorf("could not find the problem (%s/%d)", h.problemPackFile, h.problemID)
}

func (h *Handler) dumpAsJSON(data interface{}) {
	encoder := json.NewEncoder(os.Stdout)
	err := encoder.Encode(data)
	if err != nil {
		log.Fatalln(err)
	}
}
